#include "restclient.h"
#include <QFile>

RestClient::RestClient(QObject *parent) : QObject (parent) { }

QJsonObject RestClient::reply_to_json(QNetworkReply *reply)
{
    QJsonObject jsonObj;
    QJsonDocument jsonDoc;
    QJsonParseError parseError;
    auto replyText = reply->readAll();
    jsonDoc = QJsonDocument::fromJson(replyText, &parseError);
    if(parseError.error != QJsonParseError::NoError){
        qDebug() << replyText;
        qWarning() << "Json parse error: " << parseError.errorString();
    }else{
        if(jsonDoc.isObject())
            jsonObj  = jsonDoc.object();
        else if (jsonDoc.isArray())
            jsonObj["non_field_errors"] = jsonDoc.array();
    }
    return jsonObj;
}

void RestClient::send_request(int key, QString image_path)
{
    //form request
    QString api_request = "v1/detect?demographics=true"; //TODO move it to singleton
    QNetworkRequest request;
    request.setUrl(QUrl(host + (api_request)));
    request.setRawHeader("Authorization", QString("Bearer %1").arg(token).toUtf8());
    request.setHeader(QNetworkRequest::ContentTypeHeader, "image/jpeg");
    QFile *img = new QFile(image_path);
    img->open(QIODevice::ReadOnly);
    
    //send request and connect slots to request's signals
    auto reply = network_manager.post(request, img);
    connect(reply, &QNetworkReply::uploadProgress, this,
            [this, key](qint64 bytesSent, qint64 bytesTotal){
        this->uploadProgress(key, bytesSent, bytesTotal);
    });
    connect(reply, &QNetworkReply::downloadProgress, this,
            [this, key](qint64 bytesRecived, qint64 bytesTotal){
        this->downloadProgress(key, bytesRecived, bytesTotal);
    });
    connect(reply, &QNetworkReply::finished, this,
            [this, key, reply]() {
        this->reciveReply(key, reply);
        reply->close();
        reply->deleteLater();
    } );
}

void RestClient::reciveReply(int index, QNetworkReply *reply)
{
    auto json = reply_to_json(reply);
    auto annotations = json_to_annotation(json);
    emit imageProceed(index, annotations);
}

void RestClient::proceed_images(QMap<int, QString> filepaths_by_indexes)
{
    for(auto key: filepaths_by_indexes.keys())
    {
        send_request(key, filepaths_by_indexes.value(key));
    }
}

QList<Annotation> RestClient:: json_to_annotation(QJsonObject root)
{
    QList<Annotation> result;
    QJsonArray ja = root.value("data").toArray();
    for(auto i = 0; i < ja.count(); i++)
    {
        auto obj = ja.at(i).toObject();
        
        //working with box
        auto box = obj["bbox"].toObject();
        auto x = box["x"].toInt();
        auto y = box["y"].toInt();
        auto height = box["height"].toInt();
        auto width = box["width"].toInt();
        auto roi = QRect(x, y, width, height);
        //working with demographics
        auto demographics = obj["demographics"].toObject();
        auto age = demographics["age"].toObject()["mean"].toDouble();
        auto ethnicity = demographics["ethnicity"].toString();
        auto gender = demographics["gender"].toString();
        result.push_back(Annotation(roi, gender, ethnicity, age));
    }
    return result;
}
