#pragma once

#include <QObject>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <annotation.h>

class RestClient : public QObject
{
    Q_OBJECT
    
    QNetworkAccessManager network_manager;
    const QString host = "https://backend.facecloud.tevian.ru/api/";
    const QString token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI0ZGZiYzJkMC04YWE0LTQwZmYtOTg5Ny01NWZkOTFmNDU0YTAiLCJzdWIiOjMwLCJpYXQiOjE1NjE3MjMwODIsIm5iZiI6MTU2MTcyMzA4MiwidHlwZSI6ImFjY2VzcyIsImZyZXNoIjpmYWxzZX0.7EC47cZ2NhV7OO0SgW0Ik50vP-jnnwoWC-Vkeu0m6BA";    
    
    void send_request(int key, QString image_path);
    QJsonObject reply_to_json(QNetworkReply *reply);
    QList<Annotation> json_to_annotation(QJsonObject);
    void reciveReply(int index, QNetworkReply *reply);
    
public:
    RestClient(QObject *parent = nullptr);
    void proceed_images(QMap<int, QString> filepaths_by_indexes);
signals:
    void imageProceed(int index, QList<Annotation>);
    void uploadProgress(int index, qint64 bytesSent, qint64 bytesTotal);
    void downloadProgress(int index, qint64 bytesReceived, qint64 bytesTotal);
};
