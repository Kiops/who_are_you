import QtQuick 2.12
import kiops.models 1.0
import QtQuick.Controls 2.12

ApplicationWindow {
    visible: true
    width: 1920/2
    height: 1080/2
    title: qsTr("Who are you")
    
    ImageFileDialog{
        id: dialog
        onAccepted: {
            images_model.populateModel(fileUrls)
            if(swipeView.currentIndex == 0)
                swipeView.currentIndex = 1
        }
    }
    
    SwipeView{
        id: swipeView
        anchors.fill: parent
        interactive: false
        currentIndex: tabBar.currentIndex
        Page{
            Row{
                anchors.centerIn: parent
                spacing: 5
                Button{
                    text: "Load images"
                    onClicked: dialog.openFileDialog()
                }
                Button{
                    text: "Load a folder of images"
                    onClicked: dialog.openDirDialog()
                }
            }
        }
        
        Gallery{
            id: gallery
            model: images_model
            onSelectedImageChanged:  
            {
                if(selectedImage)
                {
                    fullscreenImage.model = selectedImage
                    if(swipeView.currentIndex == 1)
                        swipeView.currentIndex = 2
                }
            }
        }
        Page{
            FullscreenImage{
                anchors.fill: parent
                id: fullscreenImage
            }
            
        }
        
    }
    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
        
        TabButton {
            text: qsTr("Loading page")
            onClicked: swipeView.currentIndex = 0
        }
        TabButton {
            text: qsTr("Gallery")
            onClicked: swipeView.currentIndex = 1
            enabled: gallery.count > 0
        }
        TabButton {
            text: qsTr("Image view")
            onClicked: swipeView.currentIndex = 2
            enabled: gallery.selectedImage !== undefined
        }
    }
    
    AnnotatedImagesModel{
        id: images_model
    }
    
    
}

