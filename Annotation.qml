import QtQuick 2.12

Item {
    property var roi
    property real age
    property string gender
    property string ethnicity
    property var image
    
    Rectangle{
        id: roi_rect
        border.color: "white"
        border.width: 2
        property real horizontalSizeCoef: image.paintedWidth/image.sourceSize.width
        property real verticalSizeCoef: image.paintedHeight/image.sourceSize.height
        width: roi.width * horizontalSizeCoef + 2*border.width
        height: roi.height * verticalSizeCoef + 2*border.width
        x: roi.x * horizontalSizeCoef - border.width
        y: roi.y * verticalSizeCoef - border.width
        color: "transparent"
    }
    Rectangle{
        id: text_rect
        anchors.top: roi_rect.bottom
        anchors.left: roi_rect.left
        anchors.right: roi_rect.right
        height: demographic_text.height
        color: roi_rect.border.color
        opacity: 0.5
    }
    Text {
        id: demographic_text
        text: qsTr("%1 %2, %3 y/o").arg(ethnicity).arg(gender).arg(Math.round(age))
        anchors.left: text_rect.left
        anchors.right: text_rect.right
        anchors.verticalCenter: text_rect.verticalCenter
        horizontalAlignment: Text.AlignHCenter
        wrapMode: Text.Wrap
        opacity: 1
    }
}
