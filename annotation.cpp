#include "annotation.h"

Annotation::Annotation() { }

Annotation::Annotation(QRect roi, QString gender, QString ethnicity, double age) : m_roi(roi), m_gender(gender), m_ethnicity(ethnicity), m_age(age) {}


QVariantMap Annotation::toQVariantMap() const
{
    QVariantMap map;
    map["roi"] = m_roi;
    map["age"] = m_age;
    map["gender"] = m_gender;
    map["ethnicity"] = m_ethnicity;
    return map;
}
