#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <annotatedimagesmodel.h>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    
    QGuiApplication app(argc, argv);
    app.setOrganizationName("Kiops");
    app.setOrganizationDomain("kiops.ru");
    app.setApplicationName("Who are you");
    qmlRegisterType<AnnotatedImagesModel>("kiops.models", 1,0, "AnnotatedImagesModel");
    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);
    return app.exec();
}
