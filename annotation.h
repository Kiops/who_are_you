#pragma once
#include <QObject>
#include <QRect>
#include <QVariantMap>


class Annotation
{
    QRect m_roi;
    QString m_gender;
    QString m_ethnicity;
    double m_age;
public:
    Annotation();
    Annotation(QRect roi, QString gender, QString ethnicity, double m_age);
    QVariantMap toQVariantMap() const;
};
