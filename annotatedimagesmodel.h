#pragma once
#include <QAbstractListModel>
#include <QObject>
#include <annotation.h>
#include <restclient.h>

struct AnnotatedImage{
    QUrl path;
    QList<Annotation> annotations;
    AnnotatedImage(QUrl path): path(path){}
    double downloadRate = 0.;
    double uploadRate = 0.;
    bool proceed = false;
    QString error;
};

class AnnotatedImagesModel : public QAbstractListModel
{
    Q_OBJECT
    RestClient client;
    QList<AnnotatedImage> annotatedImages;
private slots:
    void recivedAnnotations(int index, QList<Annotation> annotations);
public:
    AnnotatedImagesModel(QObject *parent = nullptr);
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    
    enum AnnotatedImagesRoles{
        PathRole = Qt::UserRole + 1,
        AnnotationsRole,
        UploadRateRole,
        DownloadRateRole,
        ProceedRole,
        ErrorRole,
    };
    QHash<int, QByteArray> roleNames() const override;
    
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
    
    Q_INVOKABLE void populateModel(QList<QUrl> paths);
    Q_INVOKABLE QList<QVariant> getAnnotations(int index);
};
