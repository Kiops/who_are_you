import QtQuick 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Controls 2.4
import kiops.models 1.0
import QtGraphicalEffects 1.12

Page{
    id: page
    property alias model: gridView.model
    property var selectedImage
    property alias count: gridView.count
    GridView{
        id: gridView
        //        header: ProgressBar{
        //            anchors.left: parent.left
        //            anchors.right: parent.right
        //            value: 0.3
        //        }
        
        anchors.centerIn: parent
        width: parent.width*0.95
        height: parent.height*0.95
        cellWidth: width/2
        cellHeight: height/3
        delegate: Item{
            id: delegate
            width: gridView.cellWidth
            height: gridView.cellHeight
            property real loadingRate: uploadRate*0.5 + downloadRate*0.5
            Image{
                id: img
                anchors.centerIn: parent
                width: parent.width - (parent.height-height)
                height: parent.height*0.97
                source: path
                property var visualAnnotations: annotations
                fillMode: Image.PreserveAspectCrop
                Rectangle{
                    id: whiteRect
                    anchors.fill: img
                    color: "white"
                    opacity:  0.8
                    Behavior on opacity{ PropertyAnimation {duration: 500}}
                }
            }
            
            BusyIndicator{
                id: busyIndicator
                anchors.centerIn: img
                running: !proceed
                onRunningChanged: {
                    if(!running)
                    {
                        whiteRect.opacity = 0
                        progressBar.opacity = 0
                        loading_text.opacity = 0
                    }
                }
            }
            
            Text {
                id: loading_text
                text: uploadRate < 1 ? "Uploading image.." : downloadRate == 0 ? "Waiting for server.." : busyIndicator.running ? "Downloading results.." : "Done!"
                anchors.top: busyIndicator.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                opacity: 1.0
                Behavior on opacity{ PropertyAnimation {duration: 500}}
            }
            
            ProgressBar{
                id: progressBar
                anchors.left: img.left
                anchors.right: img.right
                anchors.bottom: img.bottom
                value: loadingRate
                Behavior on opacity{ PropertyAnimation {duration: 500}}
            }
            MouseArea{
                anchors.fill: parent
                onClicked: page.selectedImage = model
            }
        }
    }
}
