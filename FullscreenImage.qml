import QtQuick 2.12

Item{
    id: mainitem
    clip: true
    property var model
    property bool isZoomedOut: flick.contentWidth <= mainitem.width && flick.contentHeight <= mainitem.height
    function initialZoom()
    {
        return Math.min(mainitem.width/img.sourceSize.width, mainitem.height/img.sourceSize.height)
    }
    function initialXPosition()
    {
        return -(mainitem.width-flick.contentWidth)/2
    }
    function initialYPosition()
    {
        return -(mainitem.height-flick.contentHeight)/2
    }
    
    onModelChanged: {
        //hack to initially position photo, when other one selected
        flick.contentX = Qt.binding(function(){return -(mainitem.width-flick.contentWidth)/2})
        flick.contentY = Qt.binding(function(){return -(mainitem.height-flick.contentHeight)/2})
        isZoomedOut = Qt.binding(function(){return flick.contentWidth <= mainitem.width && flick.contentHeight <= mainitem.height})
        img.zoom = Qt.binding(function(){return Math.min(mainitem.width/img.sourceSize.width, mainitem.height/img.sourceSize.height)})
    }
    
    Flickable {
        id: flick
        anchors.fill: parent
        synchronousDrag: true
        width: parent.width
        height: parent.height
        contentWidth: img.width; contentHeight: img.height
        contentX: initialXPosition()
        contentY: initialYPosition()
        boundsBehavior: Flickable.StopAtBounds 
        interactive: !isZoomedOut
        
        Image{
            id: img
            property real zoom: initialZoom()
            width: sourceSize.width * zoom
            height: sourceSize.height * zoom
            property var annotations: model ? model.annotations : undefined
            //            fillMode: Image.Stretch
            source: model ? model.path : ""
            ListModel{
                id: repeaterModel
            }
            Repeater{
                anchors.fill: parent
                model: repeaterModel
                Annotation{
                    image: img
                    roi: model.roi
                    age: model.age
                    gender: model.gender
                    ethnicity: model.ethnicity
                }
            }
            onAnnotationsChanged: {
                repeaterModel.clear()
                for(var i in annotations){
                    repeaterModel.append({"roi": annotations[i].roi,
                                             "age": annotations[i].age,
                                             "gender": annotations[i].gender,
                                             "ethnicity": annotations[i].ethnicity
                                         })
                }
                
            }
            
        }
        MouseArea{
            anchors.fill: parent
            onWheel: {
                //saved current visible center in percents
                var xCenterPosition = flick.visibleArea.xPosition + flick.visibleArea.widthRatio/2
                var yCenterPosition = flick.visibleArea.yPosition + flick.visibleArea.heightRatio/2
                console.log("visibleCenter", flick.visibleArea.xPosition , flick.visibleArea.widthRatio/2)
                
                //change zoom according to mouse rotation
                if(wheel.angleDelta.y > 0) 
                    img.zoom *= 1.05
                else
                    if(!mainitem.isZoomedOut)
                        img.zoom /= 1.05
                
                //move image with aim to save it relative center and borders
                var get_content_coord = function(lenRatio, len, centerPos){return centerPos * len - lenRatio * len/2}
                flick.contentX = xCenterPosition * flick.contentWidth - flick.visibleArea.widthRatio * flick.contentWidth/2;
                if(flick.contentWidth <= mainitem.width)
                {
                    //image is smaller than the screen
                    flick.contentX = initialXPosition()
                }
                else if(flick.contentWidth - mainitem.width - flick.contentX < 0)
                {
                    //img is bigger than the screen, but its right border is left from right border of the screen
                    flick.contentX = flick.contentWidth - mainitem.width
                }
                else if(flick.contentX < 0)
                {
                    //img is bigger than the screen, but its left border is right from left border of the screen
                    flick.contentX = 0
                }
                
                
                //same goes for y
                flick.contentY = get_content_coord(flick.visibleArea.heightRatio, flick.contentHeight, yCenterPosition);
                if(flick.contentHeight <= mainitem.height)
                {
                    flick.contentY = initialYPosition()
                }
                else if(flick.contentHeight - mainitem.height - flick.contentY < 0)
                {
                    flick.contentY = flick.contentHeight - mainitem.height
                }
                else if(flick.contentY < 0)
                {
                    flick.contentY = 0
                }
                
            }
        }
    }
}
