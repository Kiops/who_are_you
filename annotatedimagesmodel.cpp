#include "annotatedimagesmodel.h"
#include <QFileInfo>
#include <QDebug>
#include <QDir>

AnnotatedImagesModel::AnnotatedImagesModel(QObject *parent)
    : QAbstractListModel(parent)
{
    connect(&client, &RestClient::imageProceed, this, &AnnotatedImagesModel::recivedAnnotations);    
    connect(&client, &RestClient::uploadProgress, this, [this](int index, qint64 bytesSent, qint64 bytesTotal)
    {
        double progress = bytesTotal != 0 ? double(bytesSent)/double(bytesTotal) : 1.;
        this->setData(this->index(index), progress, UploadRateRole);
    });
    connect(&client, &RestClient::downloadProgress, this, [this](int index, qint64 bytesReceived, qint64 bytesTotal)
    {
        double progress = (bytesReceived >= 0) ? double(bytesReceived)/double(bytesTotal) : 1.;
        this->setData(this->index(index), progress, DownloadRateRole);
    });
    connect(&client, &RestClient::imageProceed, this, [this](int index, QList<Annotation> annotations){
        Q_UNUSED(annotations)
        this->setData(this->index(index), true, ProceedRole);
    });
}

int AnnotatedImagesModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return annotatedImages.count();
}

QVariant AnnotatedImagesModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();
    
    if(index.row() >= annotatedImages.size() || index.row() < 0)
        return QVariant();
    
    if(role == PathRole)
        return annotatedImages.at(index.row()).path;
    
    if(role == AnnotationsRole)
    {
        QList<QVariant> list;
        auto annotations = annotatedImages.at(index.row()).annotations;
        for(auto it = annotations.begin(); it != annotations.end(); it++)
        {
            list.append(it->toQVariantMap());
        }
        return  QVariant(list);
    }
    if(role == DownloadRateRole)
        return annotatedImages.at(index.row()).downloadRate;
    if(role == UploadRateRole)
        return annotatedImages.at(index.row()).uploadRate;
    if(role == ProceedRole)
        return annotatedImages.at(index.row()).proceed;
    if(role == ErrorRole)
        return annotatedImages.at(index.row()).error;
    
    return QVariant();
}

QHash<int, QByteArray> AnnotatedImagesModel::roleNames() const
{
    QHash<int, QByteArray> role_names;
    role_names[PathRole] = "path";
    role_names[ErrorRole] = "error";
    role_names[ProceedRole] = "proceed";
    role_names[UploadRateRole] = "uploadRate";
    role_names[AnnotationsRole] = "annotations";
    role_names[DownloadRateRole] = "downloadRate";
    return role_names;
}

bool AnnotatedImagesModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(!index.isValid())
        return false;
    
    if (index.row() >= annotatedImages.size() || index.row() < 0)
        return false;
    
    if(role == PathRole)
    {
        annotatedImages[index.row()].path = value.toString();
        emit dataChanged(index, index, {PathRole});
        return true;
    }
    if(role == UploadRateRole)
    {
        annotatedImages[index.row()].uploadRate = value.toDouble();
        emit dataChanged(index, index, {UploadRateRole});
        return true;
    }
    if(role == DownloadRateRole)
    {
        annotatedImages[index.row()].downloadRate = value.toDouble();
        emit dataChanged(index, index, {DownloadRateRole});
        return true;
    }
    if(role == ErrorRole)
    {
        annotatedImages[index.row()].error = value.toString();
        emit dataChanged(index, index, {ErrorRole});
        return true;
    }
    if(role == ProceedRole)
    {
        annotatedImages[index.row()].proceed = value.toBool();
        emit dataChanged(index, index, {ProceedRole});
        return true;
    }
    return  false;
}

void AnnotatedImagesModel::populateModel(QList<QUrl> paths)
{
    //check if user's selected a directory
    if(paths.size() == 1 && QFileInfo(paths[0].path()).isDir())
    {
        QDir dir = paths[0].path();
        dir.setNameFilters({"*.jpg", "*.jpeg"});
        dir.setFilter({QDir::Files});
        auto fileList = dir.entryInfoList();
        paths.clear();
        for(auto file: fileList)
            paths.append(QUrl::fromLocalFile(file.absoluteFilePath()));
    }
    
    //add filepaths in model
    beginInsertRows(QModelIndex(), 0, paths.length()-1);
    for(auto path: paths)
    {
        annotatedImages.push_back(AnnotatedImage(path));
    }
    endInsertRows();
    
    //generate a map of indexes and urlsm send it through the client
    QMap<int, QString> map;
    for(auto i = 0; i < annotatedImages.size(); i++)
    {
        map.insert(i, annotatedImages[i].path.toLocalFile());
    }
    client.proceed_images(map);
}

QList<QVariant> AnnotatedImagesModel::getAnnotations(int index){
    QList<QVariant> annotations;
    for(auto annotation: annotatedImages[index].annotations)
        annotations.push_back(annotation.toQVariantMap());
    return  annotations;
}

void AnnotatedImagesModel::recivedAnnotations(int index, QList<Annotation> annotations)
{
    annotatedImages[index].annotations = annotations;
    emit dataChanged(this->index(index), this->index(index)/*, {AnnotationsRole}*/);
}
