import QtQuick 2.12
import QtQuick.Dialogs 1.2

Item {
    id: mainItem
    signal accepted()
    property var fileUrls
    function openFileDialog(){
        fileDialog.selectFolder = false
        fileDialog.selectMultiple = true
        fileDialog.open()
    }
    function openDirDialog(){
        fileDialog.selectMultiple = false
        fileDialog.selectFolder = true
        fileDialog.open()
    }
    FileDialog {
        id: fileDialog
        title: "Please choose image or group of images"
        folder: shortcuts.home
        selectFolder: true
        nameFilters: selectFolder ? ["*"] : [ "Image files (*.jpg *.jpeg)"]

        onAccepted: {
            mainItem.fileUrls = fileDialog.fileUrls
            mainItem.accepted()
        }
    }
}
